package com.awesome.wallpaper.entity;

import org.json.JSONArray;
import org.json.JSONObject;

public class PhotoItem {
	public String type;
	public JSONArray tags;
	public JSONArray photos;
	public JSONObject info;
	public JSONObject originalSize;

	public String imagePreview;
	public String imageOrigin;

	public String infoCamera;
	public String infoIso;
	public String infoAperture;
	public String infoExposure;
	public String infoFocalLength;

	public String getImagePreview() {
		return imagePreview;
	}

	public void setImagePreview(String imagePreview) {
		this.imagePreview = imagePreview;
	}

	public String getImageOrigin() {
		return imageOrigin;
	}

	public void setImageOrigin(String imageOrigin) {
		this.imageOrigin = imageOrigin;
	}

	public String getInfoCamera() {
		return infoCamera;
	}

	public void setInfoCamera(String infoCamera) {
		this.infoCamera = infoCamera;
	}

	public String getInfoIso() {
		return infoIso;
	}

	public void setInfoIso(String infoIso) {
		this.infoIso = infoIso;
	}

	public String getInfoAperture() {
		return infoAperture;
	}

	public void setInfoAperture(String infoAperture) {
		this.infoAperture = infoAperture;
	}

	public String getInfoExposure() {
		return infoExposure;
	}

	public void setInfoExposure(String infoExposure) {
		this.infoExposure = infoExposure;
	}

	public String getInfoFocalLength() {
		return infoFocalLength;
	}

	public void setInfoFocalLength(String infoFocalLength) {
		this.infoFocalLength = infoFocalLength;
	}

	public JSONObject getInfo() {
		return info;
	}

	public void setInfo(JSONObject info) {
		this.info = info;
	}

	public JSONObject getOriginalSize() {
		return originalSize;
	}

	public void setOriginalSize(JSONObject originalSize) {
		this.originalSize = originalSize;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public JSONArray getTags() {
		return tags;
	}

	public void setTags(JSONArray tags) {
		this.tags = tags;
	}

	public JSONArray getPhotos() {
		return photos;
	}

	public void setPhotos(JSONArray photos) {
		this.photos = photos;
	}
}
