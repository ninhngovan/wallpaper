package com.awesome.wallpaper;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.awesome.wallpaper.controller.RequestApiController;
import com.awesome.wallpaper.entity.ResultDTO;
import com.awesome.wallpaper.interfaces.IError;
import com.awesome.wallpaper.interfaces.IKey;
import com.awesome.wallpaper.interfaces.IRequestController;
import com.awesome.wallpaper.interfaces.IRequestData;
import com.awesome.wallpaper.utils.BitmapLruCache;
import com.awesome.wallpaper.utils.MyPreference;
import com.awesome.wallpaper.utils.Utils;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;

public class BaseActivity extends Activity implements IRequestController,
		IRequestData, IKey, IError {
	protected Handler handler = null;
	protected RequestApiController requestApiController = null;
	protected MyPreference myPreference = null;
	protected ImageLoader imageLoader = null;
	protected Resources resources = null;
	protected LayoutInflater inflater = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			if (requestApiController == null) {
				requestApiController = new RequestApiController(this, this);
			}

			if (handler == null) {
				handler = new Handler();
			}
			if (imageLoader == null) {
				ImageLoader.ImageCache imageCache = new BitmapLruCache();
				imageLoader = new ImageLoader(Volley.newRequestQueue(this),
						imageCache);
			}
			resources = this.getResources();
			inflater = (LayoutInflater) this
					.getSystemService(this.LAYOUT_INFLATER_SERVICE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		super.onBackPressed();
		try {
			Utils.clearHandler(handler);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * **********************************************************************
	 * Implement Function
	 * **********************************************************************
	 */
	public void onResponse(ResultDTO resultDTO) throws Exception {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onError(String message) throws Exception {
	}

}
