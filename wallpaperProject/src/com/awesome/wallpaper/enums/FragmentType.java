package com.awesome.wallpaper.enums;

public enum FragmentType {
    DO_NOTHING, STORY_LIST_FRAGMENT, STORY_DETAIL_FRAGMENT, STORY_READ_FRAGMENT, SETTING_FRAGMENT
}
