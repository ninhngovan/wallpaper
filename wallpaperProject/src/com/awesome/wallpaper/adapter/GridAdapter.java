package com.awesome.wallpaper.adapter;

import java.util.ArrayList;
import java.util.List;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.awesome.wallpaper.entity.PhotoItem;
import com.awesome.wallpaper.utils.BitmapLruCache;
import com.steam.photocomment.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class GridAdapter extends BaseAdapter {
	private List<PhotoItem> wallpapersList = new ArrayList<PhotoItem>();
	private Context context;
	private LayoutInflater inflater;
	ImageLoader imageLoader = null;

	public GridAdapter(Context context, List<PhotoItem> wallpapersList) {
		this.context = context;
		this.wallpapersList = wallpapersList;
		if (imageLoader == null) {
			ImageLoader.ImageCache imageCache = new BitmapLruCache();
			imageLoader = new ImageLoader(Volley.newRequestQueue(context),
					imageCache);
		}
	}

	@Override
	public int getCount() {
		return wallpapersList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (inflater == null)
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
			convertView = inflater.inflate(R.layout.grid_item_photo, null);

		NetworkImageView thumbNail = (NetworkImageView) convertView
				.findViewById(R.id.thumbnail);
		PhotoItem p = wallpapersList.get(position);

		thumbNail.setImageUrl(p.getImagePreview(), imageLoader);

		return convertView;
	}

}
