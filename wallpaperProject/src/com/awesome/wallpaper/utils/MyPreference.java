package com.awesome.wallpaper.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class MyPreference {
    /*
     * **********************************************************************
     * Variables
     * **********************************************************************
     */
    public final String              PREFERENCE_NAME           = "vcc.ws.mysoha";
    public final String              PREFERENCE_DEMO           = PREFERENCE_NAME + ":demo";
    public final String              PREFERENCE_USER_ID_ACTIVE = PREFERENCE_NAME + ":user_id_active";
    
    private SharedPreferences        sharedPreferences         = null;
    private SharedPreferences.Editor editor                    = null;
    
    /*
     * **********************************************************************
     * Constructor
     * **********************************************************************
     */
    public MyPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, 0);
        editor = sharedPreferences.edit();
    }
    
    /*
     * **********************************************************************
     * Save & Get
     * **********************************************************************
     */
    public String getListStringEvent() throws Exception {
        return sharedPreferences.getString(PREFERENCE_DEMO, "");
    }
    
    public void saveListStringEvent(String demo) throws Exception {
        editor.putString(PREFERENCE_DEMO, demo);
        editor.commit();
    }
    
    public String getUserIdActive() {
        return sharedPreferences.getString(PREFERENCE_USER_ID_ACTIVE, "");
    }
    
    public void saveUserIdActive(String userId) {
        editor.putString(PREFERENCE_USER_ID_ACTIVE, userId);
        editor.commit();
    }
    
    public void saveByKey(String key, Object value) throws Exception {
        if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else {
            throw new Exception("");
        }
        editor.commit();
    }
    
    public Object getByKey(String key) throws Exception {
        Object object = null;
        try {
            object = sharedPreferences.getBoolean(key, false);
        } catch (Exception e) {
            try {
                object = sharedPreferences.getInt(key, 0);
            } catch (Exception e2) {
                try {
                    object = sharedPreferences.getLong(key, 0);
                } catch (Exception e3) {
                    try {
                        object = sharedPreferences.getString(key, "");
                    } catch (Exception e4) {
                        throw new Exception("");
                    }
                }
            }
        }
        return object;
    }
}
