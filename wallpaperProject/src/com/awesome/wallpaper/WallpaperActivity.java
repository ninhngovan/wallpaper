package com.awesome.wallpaper;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.awesome.wallpaper.adapter.GridAdapter;
import com.awesome.wallpaper.entity.PhotoItem;
import com.awesome.wallpaper.entity.ResultDTO;
import com.awesome.wallpaper.enums.ActionType;
import com.awesome.wallpaper.utils.Logger;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.steam.photocomment.R;

import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;

public class WallpaperActivity extends BaseActivity {

	private final String TAG_REQUEST = WallpaperActivity.class.getSimpleName();

	private GridAdapter gridAdapter;
	private PullToRefreshGridView gridView;
	private ProgressBar loading;

	private final String itemsPage = "10";
	private int loadMore = 10;
	private final String host = "http://api.tumblr.com/v2/blog/xkcn.tumblr.com";
	private final String apiKey = "1FDhQgGAbgoAVRr4eUQJ2KhXzAkwemKs9LDbCdxklbXY6wU8rF";

	private ArrayList<PhotoItem> photosPost;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wallpaper_activity);
		gridView = (PullToRefreshGridView) findViewById(R.id.grid_view);
		loading = (ProgressBar) findViewById(R.id.pbLoader);

		gridView.setOnRefreshListener(new LoadMore());

		photosPost = new ArrayList<PhotoItem>();
		gridAdapter = new GridAdapter(this, photosPost);
		gridView.setAdapter(gridAdapter);
		getPhoto(loadMore);
	}

	private class LoadMore implements OnRefreshListener2<GridView> {

		@Override
		public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
			getPhoto(10);
		}

		@Override
		public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
			loadMore = loadMore + 10;
			getPhoto(loadMore);
		}
	}

	@Override
	public void onResponse(ResultDTO resultDTO) throws Exception {
		try {
			loading.setVisibility(View.GONE);
			gridView.onRefreshComplete();
			JSONArray arrPost = (JSONArray) resultDTO.clear().key("response")
					.key("posts").value();
			for (int i = 0; i < arrPost.length(); i++) {
				PhotoItem item = new PhotoItem();

				String type = (String) resultDTO.clear(arrPost)
						.key("type", "" + i).value();
				if (type.equals("photo")) {
					JSONArray tags = (JSONArray) resultDTO.clear(arrPost)
							.key("tags", "" + i).value();
					JSONArray photos = (JSONArray) resultDTO.clear(arrPost)
							.key("photos", "" + i).key("alt_sizes", "" + 0)
							.value();
					JSONObject info = (JSONObject) resultDTO.clear(arrPost)
							.key("photos", "" + i).key("exif", "" + 0).value();
					JSONObject originalSize = (JSONObject) resultDTO
							.clear(arrPost).key("photos", "" + i)
							.key("original_size", "" + 0).value();

					item.setType(type);
					item.setTags(tags);
					item.setPhotos(photos);
					item.setInfo(info);
					item.setOriginalSize(originalSize);

					String imagePreview = photos.getJSONObject(3).getString(
							"url");
					String imageOrigin = originalSize.getString("url");

					item.setImagePreview(imagePreview);
					item.setImageOrigin(imageOrigin);

					// String infoCamera = info.getString("Camera");
					// String infoIso = info.getString("ISO");
					// String infoAperture = info.getString("Aperture");
					// String infoExposure = info.getString("Exposure");
					// String infoFocalLength = info.getString("FocalLength");

					// item.setInfoCamera(infoCamera);
					// item.setInfoIso(infoIso);
					// item.setInfoAperture(infoAperture);
					// item.setInfoExposure(infoExposure);
					// item.setInfoFocalLength(infoFocalLength);

					photosPost.add(item);
					gridAdapter.notifyDataSetChanged();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void getPhoto(int page) {
		try {
			String url = host + "/posts?api_key=" + apiKey + "&limit="
					+ itemsPage + "&offset=" + page;

			Logger.i("ninh", "url: " + url);

			requestApiController.getRequest(url, ActionType.GET_PHOTO_LIST,
					null, TAG_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onError(String message) throws Exception {

	}

}
