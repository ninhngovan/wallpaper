package com.awesome.wallpaper.interfaces;

import com.awesome.wallpaper.entity.ResultDTO;

public interface IRequestController {
    public void onResponse(ResultDTO resultDTO) throws Exception;
    
    public void onError(String message) throws Exception;
}
